﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CallButtonTelu : MonoBehaviour {

	// Use this for initialization
	public Button[] botones;

	public void CallCerraBotones () {

		for(var i = 0; i<botones.Length; i++){

			botones [i].onClick.Invoke ();

		}

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
