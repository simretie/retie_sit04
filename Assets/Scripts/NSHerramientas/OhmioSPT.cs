﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OhmioSPT : MonoBehaviour {

	// Use this for initialization
	public float Resistencia = 0; // Debe ser puesta en porcentaje decimal
	private float MiDato;
	public float[] RandomDato; // porcentaje en entero de 1 a 3 cifras

	void Start () {


	}
	
	// Update is called once per frame
	void Update () {
		
	}public void CalcularResistencia(){

		if(RandomDato.Length>0){

			MiDato = Random.Range(RandomDato[0],RandomDato[1])/100f;

		}else{

			MiDato = Resistencia/100;

		}

	}public float GetResistencia(){

		return MiDato;

	}
}
