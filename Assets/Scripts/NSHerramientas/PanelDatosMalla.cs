﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NSBoxMessage;

public class PanelDatosMalla : MonoBehaviour {

	// Use this for initialization
	public int MyIndex = 0;
	private bool habilitar = false;
	public GameObject PanelActivar;
	public GameObject PanelOcultar;
	public panelZonas InputDatos;

	void Start () {
		
	}
	
	// Update is called once per frame
	public void Verificar (InputField inputF) {

		if (inputF.text.Length > 0 || inputF.text != "") {
			if (float.Parse (inputF.text) < 2.5f) {
		
				BoxMessageManager._instance.MtdCreateBoxMessageInfo ("El valor ingresado es inferior al establecido por el reglamento técnico RETIE, ingrese nuevamente la altura deseada.", "ACEPTAR");
		
			}
		
		} else {
		
			BoxMessageManager._instance.MtdCreateBoxMessageInfo ("Debe ingresar una medida correcta.", "ACEPTAR");


		}

	}public void MostrarMalla(int index){

		if(index == MyIndex){

			habilitar = true;

		}else{

			habilitar = false;

		}

	}public void ActivarMalla(){

		if(habilitar){
			InputDatos.gameObject.SetActive(true);
			InputDatos.Mostrar(true);
			PanelActivar.SetActive(true);
			PanelOcultar.SetActive(false);
		}
	}
}
