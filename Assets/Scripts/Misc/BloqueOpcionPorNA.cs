﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BloqueOpcionPorNA : MonoBehaviour
{

	[Header("Dropdowns")]
	[SerializeField] private Dropdown[] _opcionesRegistro = null;
	[SerializeField] private InputField[] _camposEditables = null;
	[Header("Settings")]
	[SerializeField] private int _opcionBloqueo = 3;
	[SerializeField] private bool _bloquearAlSeleccionarOpcion = true;
	[SerializeField] private bool _eliminarContenidoAlBloquear = true;

	private void Start()
	{
		EnlazarEventos();
	}

	public void EnlazarEventos()
	{
		for (int i = 0; i < _opcionesRegistro.Length; i++)
		{
			var opcionUI = _opcionesRegistro[i];
			int tempIndex = i;
			opcionUI.onValueChanged.AddListener((opcion) =>
			{
				if (!_bloquearAlSeleccionarOpcion) return;

				bool bloqueoCampo = !(opcion == _opcionBloqueo);
				DefinirInteraccion(tempIndex, bloqueoCampo);
			});
		}
	}

	public void DefinirInteraccion(int indice, bool interactuable)
	{
		Debug.Log(string.Format("Indice {0} interaccion {1}", indice, interactuable));
		if (indice < 0 || indice >= _camposEditables.Length) return;
		var campo = _camposEditables[indice];
		if (campo == null) return;

		campo.interactable = interactuable;

		if (!interactuable && _eliminarContenidoAlBloquear)
		{
			campo.text = string.Empty;
		}
	}
}
