﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class DefinicionTrafoTension
{
	[Header("Tensión 34 kV")]

	[SerializeField] private bool _aceite = false;
	[SerializeField] private bool _padMounted = false;
	[SerializeField] private bool _seco = false;

	public bool ObtenerTensionSegunTrafo(int indice)
	{
		switch (indice)
		{
			case 0: return _aceite;
			case 1: return _padMounted;
			case 2: return _seco;
			default: return false;
		}
	}

}
