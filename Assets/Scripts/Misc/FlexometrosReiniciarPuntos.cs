﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlexometrosReiniciarPuntos : MonoBehaviour
{

	[Header("Dependencias")]
	[SerializeField] private ManagerFlexometro[] _managerFlexometros = null;
	[Header("Settings")]
	[SerializeField] private bool _reiniciarPuntosEnAwake = true;

	private void Awake()
	{
		if (_reiniciarPuntosEnAwake)
			ReiniciarPuntosFlexometros(_managerFlexometros);
	}

	public void ReiniciarPuntosFlexometros(ManagerFlexometro[] managerFlexometros)
	{
		foreach (var manager in managerFlexometros)
		{
			manager.reiniciarPuntos();
		}
	}
}
