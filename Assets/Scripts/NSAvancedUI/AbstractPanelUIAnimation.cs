﻿using NSSingleton;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace NSAvancedUI
{
    /// <summary>
    /// Clase que debe ir adjunta a cada panel de la interfaz para controlar la animacion de aparicion
    /// </summary>
    [RequireComponent(typeof(CanvasGroup))]
    public abstract class AbstractPanelUIAnimation : AbstractSingleton<AbstractPanelUIAnimation>
    {
        #region members
        
        /// <summary>
        /// Curva de animacion de la escala del panel cuando la ventana se esta mostrando
        /// </summary>
        [SerializeField, Header("Animación panel"), Space(20), Tooltip("Curva de animacion de la escala del panel cuando la ventana se esta mostrando")]
        private SOAnimationsCurvePanelUI refSOAnimationsCurvePanelUI;

        /// <summary>
        /// Para controlar la transparencia del panel
        /// </summary>
        private CanvasGroup canvasGroup;

        [SerializeField, Tooltip("Tiempo en segundos en el que el panel debe aparecer por completo")]
        /// <summary>
        /// Tiempo en segundos en el que el panel debe aparecer por completo
        /// </summary>
        private float tiempoAparicion;

        [SerializeField, Tooltip("Tiempo en segundos en el que el panel debe ocultarse por completo")]
        /// <summary>
        /// Tiempo en segundos en el que el panel debe ocultarse por completo
        /// </summary>
        private float tiempoOcultacion;

        private IEnumerator couAparecer;

        private IEnumerator couOcultar;

        [SerializeField, Header("Panel fondo oscuro")]
        private GameObject prefabPanelImagenFondoVentanaObscura;

        private GameObject panelImagenFondoActivo;

        [SerializeField]
        private bool crearImagenFondoOscuro;
        #endregion

        #region events

        [Header("Eventos"), Space(20)]

        /// <summary>
        /// Ejecutar evento cuando el panel aparece?
        /// </summary>
        [SerializeField, Tooltip("Ejecutar evento cuando el panel aparece?")]
        private bool ejecutarEventoAparicion;

        /// <summary>
        /// Se ejecuta cuando el panel completa su aparicion
        /// </summary>
        [SerializeField, Tooltip("Se ejecuta cuando el panel completa su aparicion")]
        private UnityEvent OnPanelAparecio;

        /// <summary>
        /// Ejecutar evento cuando el panel se oculta?
        /// </summary>
        [SerializeField, Tooltip("Ejecutar evento cuando el panel se oculta?")]
        private bool ejecutarEventoOcultacion;

        /// <summary>
        /// Se ejecuta cuando el panel completa su ocultacion
        /// </summary>
        [SerializeField, Tooltip("Se ejecuta cuando el panel completa su ocultacion")]
        private UnityEvent OnPanelOculto;
        #endregion

        #region public methods

        /// <summary>
        /// Asigna el bool que permite que se pueda ejecutar el evento que notifica que debe ejecutarse el evento de ocultacion
        /// </summary>
        /// <param name="argEjecutarEvento">Ejecutar el evento de ocultacion? </param>
        public void EjecutarEventoOcultacion(bool argEjecutarEvento)
        {
            ejecutarEventoOcultacion = argEjecutarEvento;
        }

        /// <summary>
        /// Ejecuta la animacion del panel para este aparesca o se oculte
        /// </summary>
        /// <param name="argMostrar">Mostrar el panel? o ocultarlo?</param>
        public void Mostrar(bool argMostrar = true)
        {
            if (canvasGroup == null)
                canvasGroup = GetComponent<CanvasGroup>();

            if (argMostrar)
            {
                if (couAparecer == null)
                {
                    gameObject.SetActive(true);
                    StopAllCoroutines();
                    couOcultar = null;
                    couAparecer = CouAparecer();
                    StartCoroutine(couAparecer);

                    if (crearImagenFondoOscuro)
                    {
                        panelImagenFondoActivo = Instantiate(prefabPanelImagenFondoVentanaObscura, transform.parent);
                        panelImagenFondoActivo.GetComponent<Transform>().SetSiblingIndex(transform.GetSiblingIndex());
                    }
                }
                else
                    Debug.LogWarning("El panel ya esta ejecutando una animacion para aparecer");
            }
            else
            {
                if (!gameObject.activeSelf)
                    return;

                if (couOcultar == null)
                {
                    StopAllCoroutines();
                    couAparecer = null;
                    couOcultar = CouOcultar();
                    StartCoroutine(CouOcultar());

                    if (crearImagenFondoOscuro)
                        if (panelImagenFondoActivo)
                            Destroy(panelImagenFondoActivo);
                }
                else
                    Debug.LogWarning("El panel ya esta ejecutando una animacion para ocultarse");
            }
        }
        #endregion

        #region courutines

        /// <summary>
        /// Courutina que ejecuta la animacion que hace aparecer el panel
        /// </summary>
        private IEnumerator CouAparecer()
        {
            var tmpTiempoAnimacion = 0f;
            var tmpRectTransform = GetComponent<RectTransform>();

            while (tmpTiempoAnimacion <= tiempoAparicion)
            {
                tmpTiempoAnimacion += Time.deltaTime;
               // Debug.Log(refSOAnimationsCurvePanelUI);
                var tmpTiempoMaximoCurvaAnimacionEscala = refSOAnimationsCurvePanelUI._animationCurveEscalaAparecer.keys[refSOAnimationsCurvePanelUI._animationCurveEscalaAparecer.keys.Length - 1].time;
                tmpRectTransform.localScale = Vector3.one * refSOAnimationsCurvePanelUI._animationCurveEscalaAparecer.Evaluate((tmpTiempoAnimacion * tmpTiempoMaximoCurvaAnimacionEscala) / tiempoAparicion);
                var tmpTiempoMaximoCurvaAnimacionAlpha = refSOAnimationsCurvePanelUI._animationCurveTransparenciaAparecer.keys[refSOAnimationsCurvePanelUI._animationCurveTransparenciaAparecer.keys.Length - 1].time;
                canvasGroup.alpha = refSOAnimationsCurvePanelUI._animationCurveTransparenciaAparecer.Evaluate((tmpTiempoAnimacion * tmpTiempoMaximoCurvaAnimacionAlpha) / tiempoAparicion);
                yield return null;

            }

            if (ejecutarEventoAparicion)
                OnPanelAparecio.Invoke();

            couAparecer = null;
        }

        /// <summary>
        /// Courutina que ejecuta la animacion que hace ocultar el panel
        /// </summary>
        private IEnumerator CouOcultar()
        {
            var tmpTiempoAnimacion = 0f;
            var tmpRectTransform = GetComponent<RectTransform>();

            while (tmpTiempoAnimacion <= tiempoOcultacion)
            {
                tmpTiempoAnimacion += Time.deltaTime;
                var tmpTiempoMaximoCurvaAnimacionEscala = refSOAnimationsCurvePanelUI._animationCurveEscalaOcultar.keys[refSOAnimationsCurvePanelUI._animationCurveEscalaOcultar.keys.Length - 1].time;
                tmpRectTransform.localScale = Vector3.one * refSOAnimationsCurvePanelUI._animationCurveEscalaOcultar.Evaluate((tmpTiempoAnimacion * tmpTiempoMaximoCurvaAnimacionEscala) / tiempoOcultacion);
                var tmpTiempoMaximoCurvaAnimacionAlpha = refSOAnimationsCurvePanelUI._animationCurveTransparenciaOcultar.keys[refSOAnimationsCurvePanelUI._animationCurveTransparenciaOcultar.keys.Length - 1].time;
                canvasGroup.alpha = refSOAnimationsCurvePanelUI._animationCurveTransparenciaOcultar.Evaluate((tmpTiempoAnimacion * tmpTiempoMaximoCurvaAnimacionAlpha) / tiempoOcultacion);
                yield return null;
            }

            gameObject.SetActive(false);
            Debug.Log("CouOcultar");

            if (ejecutarEventoOcultacion)
                OnPanelOculto.Invoke();

            couOcultar = null;
        }
        #endregion
    }
}