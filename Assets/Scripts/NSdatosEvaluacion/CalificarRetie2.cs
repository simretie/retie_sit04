﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using NSBoxMessage;
using NSInterfaz;
using System;
using System.Globalization;
using NSInterfaz;
using NSInterfazAvanzada;


public class CalificarRetie2 : MonoBehaviour
{

	[Header("Inputs Usuario")]
	public InputField[] Longitudes;
	public InputField[] ResistenciasMaximas;
	public InputField[] DistanciasDVarilla;
	public InputField[] ResistenciasFinal;

	public float[] MaxOhmioZonas;
	public float[] ResistenciasFinales; // el valor del 61.8%
	public float[] DistanciasDVarillaTierra; // 15.86 para zona 0,2,3, la zona 1 se calcula con el heigth random * 6.5
	public float longitud = 2.44f; //distancia a la que deben estar las varillas que es 2.44 
	public bool EmptyCampos = false;
	public Graficadora ControlHeight; // la graficado es la que genera el random del height para la zona 2

	// holded data
	private float[] MaxResistenciaZonas = new float[4];
	private float[] MediaResistenciaZonas = new float[4];
	private bool[] CamposCorrectos = new bool[16];
	public GameObject[] VentanasDistancias;
	public TMPro.TextMeshProUGUI[] textosABlanco;
	public TMPro.TMP_InputField[] inputs;
	public bool EnZona0Error = false;

	public float NotaTotal = 0f;

	public bool bientodos = true;

	public float VerificarDatosUsuario(){

		float Calificacion = 0f;

		EmptyCampos = false;
		bientodos = false;
		CamposCorrectos = new bool[16];
		Longitudes [1].gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = false;

		CamposCorrectos [1] = true;
		for (var i = 0; i < 4; i++) {

			// campos vacíos
			if(Longitudes[i].text == "" || Longitudes[i].text.Length ==0 && i!=1){

				EmptyCampos = true;

			}if(ResistenciasMaximas[i].text == "" || ResistenciasMaximas[i].text.Length == 0){

				EmptyCampos = true;

			}if (DistanciasDVarilla [i].text == "" || DistanciasDVarilla [i].text.Length == 0  && i != 1) {
			
				EmptyCampos = true;
			
			}if(ResistenciasFinal[i].text == ""){

				EmptyCampos = true;

			}



		
			if (Longitudes [i].text == "2.44" && i != 1) {
			
				CamposCorrectos[i] = true;
				Longitudes [i].gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = false;
			
			//}if(ResistenciasMaximas[i].text == MaxResistenciaZonas[i].ToString()){
			}else{

				Longitudes [i].gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = true;

			}if(ResistenciasMaximas[i].text == MaxOhmioZonas[i].ToString()){
				
				CamposCorrectos [i + 4] = true;
				ResistenciasMaximas [i].gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = false;

			}else{

				ResistenciasMaximas [i].gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = true;

			}if(DistanciasDVarilla[i].text == "15.86" && i!= 1){

				CamposCorrectos [i + 8] = true;
				DistanciasDVarilla [i].gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = false;

			}else{

				DistanciasDVarilla [i].gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = true;

			}if (i == 1 && DistanciasDVarilla [i].text.Length >0 && (float.Parse(DistanciasDVarilla [i].text).ToString("F1")) == ((float)ControlHeight.GetHeightValue () * 6.5f).ToString ("F1")) {
			
				CamposCorrectos [i + 8] = true;
				DistanciasDVarilla [1].gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = false;
			
			}else if(i == 1 && DistanciasDVarilla [i].text.Length >0 && (float.Parse(DistanciasDVarilla [i].text).ToString("F1")) != ((float)ControlHeight.GetHeightValue () * 6.5f).ToString ("F1")){

				DistanciasDVarilla [1].gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = true;

			}if(ResistenciasFinal[i].text == MediaResistenciaZonas[i].ToString()){

				CamposCorrectos [i + 12] = true;
				ResistenciasFinal [i].gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = false;

			}else{

				ResistenciasFinal [i].gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = true;

			}if (MediaResistenciaZonas [i] < MaxOhmioZonas [i] && MediaResistenciaZonas [i] > 1) {// SPT-Correcto
			
				Calificacion += 0.075f;
			
			}

			// zona 0 que debe dar error
			if(ResistenciasFinal[0].text.Length > 0){

				if (float.Parse (ResistenciasFinal [0].text) > 20) {
				
					CamposCorrectos [i + 12] = false;
					ResistenciasFinal [i].gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = true;

				}if(EnZona0Error){

					CamposCorrectos [i + 12] = false;
					ResistenciasFinal [i].gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = true;

				}
			
			}

		
		}

		bientodos = true;
		for (var i = 0; i < CamposCorrectos.Length; i++) {

			if (CamposCorrectos [i]) {
			
				Calificacion += 0.03125f;

			} else {
			
				bientodos = false;
			
			}
		
		}

		return Calificacion;

	}public void SetMaxResistenciaZona(int zona, float valor){

		MaxResistenciaZonas[zona] = valor;

	}public void SetResistenciaMedia(int zona, float valor){

		MediaResistenciaZonas[zona] = valor;

	}public void reinicarCampos(){

		NotaTotal = 0;
		MaxResistenciaZonas = new float[4];
		MediaResistenciaZonas = new float[4];
		CamposCorrectos = new bool[16];

		for(var i = 0; i < 4; i++){

			Longitudes[i].text = "";
			ResistenciasMaximas[i].text = "";
			DistanciasDVarilla[i].text = "";
			ResistenciasFinal[i].text = "";

			Longitudes [i].gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = false;
			ResistenciasMaximas [i].gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = false;
			DistanciasDVarilla [i].gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = false;
			ResistenciasFinal [i].gameObject.transform.Find ("icono_error").GetComponent<Image> ().enabled = false;


		}
 	
		for(int i = 0; i< VentanasDistancias.Length; i++){

			VentanasDistancias[i].SetActive(false);

		}
		for(int i = 0; i< textosABlanco.Length; i++){
			
			textosABlanco[i].text = "";

		}
		for(int i = 0; i< inputs.Length; i++){
		
			inputs[i].text = "";
		
		}

	}



}
