﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSInterfaz;
using NsSeguridad;
public class ControladorDeDatos : MonoBehaviour {

    #region members


    private byte cantidadIntentos;

    private float tiempoSituacion;

    private bool contarTiempoSituacion;

    private ClsSeguridad Seguridad;

    private string[] datoSesionUsuaro;

    public PanelBarraUsuario PanelUsuario;


    public delegate void delegateTiempoSituacion(float argTiempoSituacionActual);

    public delegate void delegateCantidadIntentos(byte argCantidadIntentosActuales);

    private delegateTiempoSituacion dltTiempoSituacion = delegate (float argTiempoSituacionActual)
    { };

    private delegateCantidadIntentos dltCantidadIntentos = delegate (byte argCantidadIntentosActuales)
    { };

    #endregion

    #region accesors

    public byte _cantidadIntentos
    {
        get
        {
            return cantidadIntentos;
        }
    }

    public float _tiempoSituacion
    {
        get
        {
            return tiempoSituacion;
        }
    }

    public delegateTiempoSituacion _listenerTiempoSituacion
    {
        get
        {
            return dltTiempoSituacion;
        }
        set
        {
            dltTiempoSituacion = value;
        }
    }

    public delegateCantidadIntentos _listenerCantidadIntentos
    {
        get
        {
            return dltCantidadIntentos;
        }
        set
        {
            dltCantidadIntentos = value;
        }
    }

    #endregion

    #region monoBehaviour

    private void Start()
    {
        Seguridad = GameObject.FindGameObjectWithTag("Seguridad").GetComponent<ClsSeguridad>();
        IniciarNuevaSesionSituacion();
        StartCoroutine(actualizarTimepo());
        SetContarTiempo(true);
    }

    // Update is called once per frame
    void Update()
    {
        ContarTiempo();
        
    }
    #endregion

    #region private methods

    private void ContarTiempo()
    {
        if (contarTiempoSituacion)
        {
            tiempoSituacion += Time.deltaTime;
            dltTiempoSituacion(tiempoSituacion);
        }
    }
    #endregion

    #region public methods
    /// <summary>
    /// reinicia oa ctiva al cuenta de tiempo en a practica
    /// </summary>
    /// <param name="argContarTiempo"></param>
    public void SetContarTiempo(bool argContarTiempo)
    {
        contarTiempoSituacion = argContarTiempo;
        tiempoSituacion = 0f;
        dltTiempoSituacion(tiempoSituacion);
    }

    public void AddIntentos()
    {
        cantidadIntentos++;
        // var aux = clsEvalucaion._refCalificacionSituacion;
        // aux._cantidadIntentos = cantidadIntentos;
        PanelUsuario.TextIntentos.text = cantidadIntentos.ToString();
        dltCantidadIntentos(cantidadIntentos);
    }

    public void IniciarNuevaSesionSituacion()
    {
        datoSesionUsuaro = Seguridad.GetDatosSesion();
        PanelUsuario.NombreUser.text = datoSesionUsuaro[0];
        SetContarTiempo(true);
        cantidadIntentos = 0;
        PanelUsuario.TextIntentos.text = cantidadIntentos.ToString();
        dltCantidadIntentos(cantidadIntentos);
    }


    #endregion

    IEnumerator actualizarTimepo()
    {
        while (contarTiempoSituacion)
        {
            ActualizarValorTiempoSesion(tiempoSituacion);
            yield return new WaitForSeconds(1f);
        }
    }

    public void ActualizarValorTiempoSesion(float argTiempoSesionActual)
    {
        var tmpSegundos = Mathf.Floor(argTiempoSesionActual % 60);
        var tmpMinutos = Mathf.Floor(argTiempoSesionActual / 60);
        PanelUsuario.TimepoPractica.text = ((tmpMinutos < 10f) ? "0" + tmpMinutos.ToString() : tmpMinutos.ToString()) + ":" + ((tmpSegundos < 10f) ? "0" + tmpSegundos.ToString() : tmpSegundos.ToString());

    }

}
