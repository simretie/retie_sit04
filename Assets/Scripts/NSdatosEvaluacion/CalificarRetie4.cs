﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CalificarRetie4 : MonoBehaviour {

	// Use this for initialization
	[Header("Aleatorio posibilidad")]
	[SerializeField] private int _minPosibilidad = 0;
	[SerializeField] private int _maxPosibilidad = 3;
	public GameObject[] Opciones;
	public GameObject[] inputsDistancias;
	public SelectorRetie3[] Selectores;
	public float[] distanciasTrafo34Kv;
	public float[] distanciasTrafo13Kv;
	public int[] RangosDistancias;
	public GameObject[] Flexometros;
	public PuntoFLexometro[] PuntosFlexo;
	public int FlexoInvertido = 1; // el flexometro que se le suma 0.5
	public bool CamposCorrectos = false;
	public bool EmptyCampos = false;

	[Header("Posibilidad 1")]
	public int[] RespuestaCorrecta1;
	public bool[] indexInverso1;
	public int[] EstadoInicial1;
	public bool[] HabilitarEdicion1;
	public DefinicionTrafoTension _definicionTrafoTension1;

	[Header("Posibilidad 2")]
	public int[] RespuestaCorrecta2;
	public bool[] indexInverso2;
	public int[] EstadoInicial2;
	public bool[] HabilitarEdicion2;
	public DefinicionTrafoTension _definicionTrafoTension2;

	[Header("Posibilidad 3")]
	public int[] RespuestaCorrecta3;
	public bool[] indexInverso3;
	public int[] EstadoInicial3;
	public bool[] HabilitarEdicion3;
	public DefinicionTrafoTension _definicionTrafoTension3;

	[Header("Posibilidad 4")]
	public int[] RespuestaCorrecta4;
	public bool[] indexInverso4;
	public int[] EstadoInicial4;
	public bool[] HabilitarEdicion4;
	public DefinicionTrafoTension _definicionTrafoTension4;

	public float Calificacion = 0f;
	public float NotaTotal = 0f;

	private int[][] Resultado = new int[4][];
	private bool[][] HabilitarEdicion = new bool[4][];
	public GameObject[] ZonasActivasBotones;

	[Header("Trafos")]

	public GameObject[] Trafos1; // placas internas en cada trafo con los datos diferentes
	public GameObject[] Trafos2;
	public GameObject[] Trafos3;
	public GameObject[] Trafos4;
	private GameObject[][] TrafosList = new GameObject[4][];

	[Header("Opciones de reinicio")]
	public GameObject[] ZonasPrincipales;
	public GameObject[] ZonasActivas;
	private int[][] SettingsList = new int[4][];
    [Header("Opcion de configuracion")]
    public int randomSettings = 0;
	public GameObject[] Trafos;
	private int flexoHabilitado = 0;
	private int PotenciaTransformador = 0; // 1 para 34 kv

	void Start () {


        randomSettings = Random.Range(_minPosibilidad, _maxPosibilidad + 1);
		SettingsList [0] = EstadoInicial1;
		SettingsList [1] = EstadoInicial2;
		SettingsList [2] = EstadoInicial3;
		SettingsList [3] = EstadoInicial4;

		HabilitarEdicion [0] = HabilitarEdicion1;
		HabilitarEdicion [1] = HabilitarEdicion2;
		HabilitarEdicion [2] = HabilitarEdicion3;
		HabilitarEdicion [3] = HabilitarEdicion4;

		for(var i =0; i<Selectores.Length; i++){

			Selectores[i].RandomStart(SettingsList[randomSettings][i]);
			Selectores [i].EnableMod (HabilitarEdicion[randomSettings][i]);

		}

		//Habilitar Cajas
		for(var i = 0; i< ZonasActivasBotones.Length; i++){

			ZonasActivasBotones[i].SetActive(HabilitarEdicion[randomSettings][i]);


		}

		TrafosList [0] = Trafos1;
		TrafosList [1] = Trafos2;
		TrafosList [2] = Trafos3;
		TrafosList [3] = Trafos4;

		for(var i = 0; i<TrafosList.Length; i++){

 			for(var l = 0; l<Trafos1.Length; l++){

				if (i == randomSettings) {
					TrafosList [randomSettings] [l].SetActive (true); 
				} else {

					TrafosList [i] [l].SetActive (false);
				
				}
			}
		}
       /* if ()
        {
            ZonasActivasBotones[i].
        }*/
	}
	
	// Update is called once per frame
	public void Varificar () {

		CompararConfiguracion ();

	}
	public void CompararConfiguracion()
	{

		CamposCorrectos = false;
		EmptyCampos = false;

		Calificacion = 0;

		Resultado[0] = new int[Selectores.Length];
		Resultado[1] = new int[Selectores.Length];
		Resultado[2] = new int[Selectores.Length];
		Resultado[3] = new int[Selectores.Length];

		CompararConfiguracionPosibilidad(RespuestaCorrecta1, indexInverso1, ref Resultado[0]);
		CompararConfiguracionPosibilidad(RespuestaCorrecta2, indexInverso2, ref Resultado[1]);
		CompararConfiguracionPosibilidad(RespuestaCorrecta3, indexInverso3, ref Resultado[2]);
		CompararConfiguracionPosibilidad(RespuestaCorrecta4, indexInverso4, ref Resultado[3]);

		ContarResupuesta();

	}

	private void CompararConfiguracionPosibilidad(int[] respuestasCorrectas, bool[] indexInverso, ref int[] resultados)
	{
		List<int> listaCablesSuperior = new List<int>()
		{
			10,9,8 // Colores superiores (alta)
		};
		List<int> listaCablesInferior = new List<int>()
		{
			13,12 // Colores inferiores (baja)
		};

		// opción1
		for (var i = 0; i < Selectores.Length; i++)
		{
			int actualSeleccionado = Selectores[i].ActualSeleccionado;
			int seleccionCorrecta = respuestasCorrectas[i];
			bool seCalificaPorDiferente = indexInverso[i];

			if (listaCablesInferior.Contains(i) || listaCablesSuperior.Contains(i))
			{
				// Es una comparación de cables de colores

				// Verifico que el indice que estoy comparando
				// Sea el índice de cable que necesito comparar
				// ------ - Superior - Inferior
				// Aceite||    10    ||    13
				// Pad   ||    9    ||    12
				// Seco  ||   8    ||    NA

				// El indice del cable es el indice correspondiente al transformador en la parte inferior o superior

				// Verifico si el transformador es del tipo correcto
				int tipoTransformadorSeleccionado = Selectores[7].ActualSeleccionado;
				int tipoTransformadorCorrecto = respuestasCorrectas[7];
				bool seCalificaDiferenteTrafo = indexInverso[7];

				if (tipoTransformadorSeleccionado == tipoTransformadorCorrecto || (tipoTransformadorSeleccionado != tipoTransformadorCorrecto && seCalificaDiferenteTrafo))
				{
					// Verifico que el cable esté en la zona inferior o superior
					if (listaCablesInferior.Contains(i))
					{
						// Cableado inferior

						// Si NO estoy revisando el transformador seco (Sin cables inferiores) 
						// y el cable corresponde al transformador que estoy verificando 
						// o es el transformador tipo seco, que no tiene cables inferiores (Sin la verificación de índice de cable)
						if ((PotenciaTransformador != 2 && i == listaCablesInferior[PotenciaTransformador]) || PotenciaTransformador == 2)
						{
							// Si es igual al transformador seco
							// Que no tiene cables inferiores
							if (PotenciaTransformador == 2)
							{
								foreach (var indiceCableInferior in listaCablesInferior)
								{
									resultados[indiceCableInferior] = 1;
								}
							}
							else
							{
								// Si el transformador aplica para cables inferiores
								int calificacion = 0;
								if (actualSeleccionado == seleccionCorrecta || (actualSeleccionado == seleccionCorrecta && seCalificaPorDiferente))
								{
									// Calificación correcta es igual a 1
									calificacion = 1;
								}

								// Defino todos los cables inferiores con la calificación
								foreach (var indiceCableInferior in listaCablesInferior)
								{
									resultados[indiceCableInferior] = calificacion;
								}
							}
						}
					}
					else
					{
						// Cableado superior

						// Si el cable corresponde al transformador que estoy verificando
						if (i == listaCablesSuperior[PotenciaTransformador])
						{
							int calificacion = 0;
							if (actualSeleccionado == seleccionCorrecta || (actualSeleccionado != seleccionCorrecta && seCalificaPorDiferente))
							{
								// Calificación correcta es igual a 1
								calificacion = 1;
							}

							// Defino todos los cables superiores con la calificación
							foreach (var indiceCableSuperior in listaCablesSuperior)
							{
								resultados[indiceCableSuperior] = calificacion;
							}
						}
					}
				}
				else
				{
					// Califico todos los cables como malos
					foreach (var indiceCableInferior in listaCablesInferior)
					{
						resultados[indiceCableInferior] = 0;
					}

					foreach (var indiceCableSuperior in listaCablesSuperior)
					{
						resultados[indiceCableSuperior] = 0;
					}
				}
			}
			else
			{
				if (actualSeleccionado == seleccionCorrecta)
				{ // 
					resultados[i] = 1;
				}
				else if (actualSeleccionado != seleccionCorrecta && seCalificaPorDiferente)
				{ // si hay más de dos opciones, la correcta de la lista es la incorrecta
					resultados[i] = 1;
				}
				else
				{
					resultados[i] = 0;
				}
			}
		}
	}


	public void ContarResupuesta()
	{

		float notaPorVista = 0.45f / (float)Selectores.Length;
		float notaTotalVistas = 0f;

		foreach (var resultado in Resultado[randomSettings])
		{
			if (resultado == 1)
			{
				notaTotalVistas += notaPorVista;
			}
		}

		Debug.Log(string.Format("Nota total vistas: {0}", notaTotalVistas));
		Calificacion += notaTotalVistas;
		CompararMedidas();
			
	}public void SetFlexoHabilitado(int flexo){

		// saber el estado inicial de los flexometros para activar los inputs correctos
		flexoHabilitado = flexo;

	}public void SetPotenciaTransformador(int Pot){

		// saber el estado inicial de los flexometros para activar los inputs correctos
		PotenciaTransformador = Pot;

	}

	public bool ValidarInputEntrada(string input, float valor, float margenError = 0.01f)
	{
		float numeroInput;
		bool conversionValida = System.Single.TryParse(input, out numeroInput);
		if (conversionValida)
		{
			float diferenciaAbsoluta = Mathf.Abs(numeroInput - valor);
			return diferenciaAbsoluta <= margenError;
		}
		return false;
	}

	public DefinicionTrafoTension ObtenerPorConfiguracion(int posibilidadElegida)
	{
		switch (posibilidadElegida)
		{
			case 0: return _definicionTrafoTension1;
			case 1: return _definicionTrafoTension2;
			case 2: return _definicionTrafoTension3;
			case 3: return _definicionTrafoTension4;
			default: return null;
		}
	}
	public void CompararMedidas()
	{

		// Existe encerramiento en transformador
		bool usandoMedidasFrenteVivo = flexoHabilitado != FlexoInvertido;

		EmptyCampos = false;
		CamposCorrectos = true;
		float notaPorCampo = 0.01563f;
		float NotaTotalCampos = 0f;

		float[] distanciasDeLosPuntos = new float[8];

		// Obtengo las medidas dadas por los flexómetros
		bool[] distanciaCumpleConRetie = new bool[4];

		float[] distanciasMedidasConFlexo = new float[4];

		// Obtengo las medidas usadas
		int indiceFlexometro = flexoHabilitado * 4;
		for (int i = 0; i < 4; i++)
			distanciasMedidasConFlexo[i] = PuntosFlexo[indiceFlexometro + i].getVolorObtenidoAleatoriaMente();

		DefinicionTrafoTension definicionTension = ObtenerPorConfiguracion(randomSettings);
		bool usoTrafo34Kv = definicionTension.ObtenerTensionSegunTrafo(PotenciaTransformador);

		bool[] opcionNACorrecta = new bool[4];

		for (var i = 0; i < 4; i++)
		{
			Debug.Log(string.Format("Distancia de flexo D{0} corresponde a {1}", (flexoHabilitado == 0 ? 4 : 0) + i + 1, distanciasMedidasConFlexo[i]));
			distanciaCumpleConRetie[i] = System.Math.Round(distanciasMedidasConFlexo[i], 2) - (usoTrafo34Kv ? distanciasTrafo34Kv[i] : distanciasTrafo13Kv[i]) >= -0.001f;

			// La seccion uno corresponde a los campos de la parte superior (del 1 hasta el 4)
			// Los campos a la derecha de la opción correspondiente
			Dropdown opcionS1 = Opciones[i].transform.GetComponentInChildren<Dropdown>();
			Image iconoErrorOpcionS1 = Opciones[i].transform.Find("icono_error").GetComponent<Image>();
			Image iconoErrorCampoS1 = inputsDistancias[i].transform.Find("icono_error").GetComponent<Image>();

			// La seccion dos corresponde a los campos de la parte inferior (del 5 hasta el 8)
			// Los campos a la derecha de la opción correspondiente
			Dropdown opcionS2 = Opciones[i + 4].transform.GetComponentInChildren<Dropdown>();
			Image iconoErrorOpcionS2 = Opciones[i + 4].transform.Find("icono_error").GetComponent<Image>();
			Image iconoErrorCampoS2 = inputsDistancias[i + 4].transform.Find("icono_error").GetComponent<Image>();

			// Evaluo las opciones de selección en el registro de datos
			if (usandoMedidasFrenteVivo)
			{

				if (opcionS1.value == 0)
					EmptyCampos = true;

				if (opcionS1.value == 1 && distanciaCumpleConRetie[i])
				{// correcto
					iconoErrorOpcionS1.enabled = false;
					NotaTotalCampos += notaPorCampo;
				}
				else if (opcionS1.value == 2 && !distanciaCumpleConRetie[i])
				{// correcto
					iconoErrorOpcionS1.enabled = false;
					NotaTotalCampos += notaPorCampo;
				}else{
					iconoErrorOpcionS1.enabled = true;
					CamposCorrectos = false;
				}
				// las opciones del NA
				if (opcionS2.value == 3)
				{// correcto
					iconoErrorOpcionS2.enabled = false;
					iconoErrorCampoS2.enabled = false;
					NotaTotalCampos += notaPorCampo;
					opcionNACorrecta[i] = true;
 				}else{
					iconoErrorOpcionS2.enabled = true; // incorrecto
					iconoErrorCampoS2.enabled = true;
					//CamposCorrectos = false;
				}
			}

			if (!usandoMedidasFrenteVivo)
			{

				if (opcionS2.value == 0)
					EmptyCampos = true;

				// las opciones del NA
				if (opcionS1.value == 3)
				{// correcto
					iconoErrorOpcionS1.enabled = false;
					iconoErrorCampoS1.enabled = false;
					NotaTotalCampos += notaPorCampo;
					opcionNACorrecta[i] = true;
 				}else{
					iconoErrorOpcionS1.enabled = true; // incorrecto
					iconoErrorCampoS1.enabled = true;
					CamposCorrectos = false;
				}

				if (opcionS2.value == 1 && distanciaCumpleConRetie[i])
				{// correcto
					iconoErrorOpcionS2.enabled = false;
					NotaTotalCampos += notaPorCampo;
				}
				else if (opcionS2.value == 2 && !distanciaCumpleConRetie[i])
				{// correcto
					iconoErrorOpcionS2.enabled = false;
					NotaTotalCampos += notaPorCampo;
				}else{
					iconoErrorOpcionS2.enabled = true;
					CamposCorrectos = false;
				}
			}
		}

		// verificación de las distancias correcta en los inputs
		for (var i = 0; i < inputsDistancias.Length; i++)
		{
			Image iconoError = inputsDistancias[i].transform.Find("icono_error").GetComponent<Image>();
			InputField inputField = inputsDistancias[i].GetComponentInChildren<InputField>();
			iconoError.enabled = false;

			// Los campos NA son los primeros 4 valores
			if (!usandoMedidasFrenteVivo)
			{
				// Primeros 4 indices
				if (i >= 0 && i <= 3)
				{
					// La opción NA es correcta en el índice
					if (opcionNACorrecta[i])
					{
						// CantidadDistanciaMin += 1;
						// minCorrectos += 1;

						// Aumento la nota
						NotaTotalCampos += notaPorCampo;
						continue;
					}
				}
			}
			else
			{
				// Los campos NA son los ultimos 4 valores

				// Recorro los índices de las opciones NA
				if (i >= 4 && i <= 7)
				{
					// La opción NA es correcta en el índice
					if (opcionNACorrecta[i - 4])
					{
						// Aumento la nota
						NotaTotalCampos += notaPorCampo;
						continue;
					}
				}
			}

				string textoInput = inputField.text;
			int indiceMedida = i % distanciasTrafo34Kv.Length;
			float distanciaCorrecta = usoTrafo34Kv ? distanciasTrafo34Kv[indiceMedida] : distanciasTrafo13Kv[indiceMedida];
				if (ValidarInputEntrada(textoInput, distanciaCorrecta))
				{
					iconoError.enabled = false;
					NotaTotalCampos += notaPorCampo;
				}
				else
				{
					iconoError.enabled = true;
					CamposCorrectos = false;
				}
		}

		NotaTotalCampos = Mathf.Min(0.25f, NotaTotalCampos);

		Calificacion += NotaTotalCampos;
		Debug.Log(string.Format("Nota total campos: {0}", NotaTotalCampos));
		NotaTotal = Calificacion;

	}public float VerificarDatosUsuario(){

		Varificar ();
		return NotaTotal;

	}public void Reiniciar(){

		randomSettings = Random.Range(_minPosibilidad, _maxPosibilidad + 1);
		for(var i = 0; i<ZonasPrincipales.Length; i++){

			ZonasPrincipales[i].gameObject.SetActive(true);

		}for(var i =0; i<Selectores.Length; i++){

			Selectores[i].RandomStart(SettingsList[randomSettings][i]);
			Selectores [i].EnableMod (HabilitarEdicion[randomSettings][i]);
			Selectores[i].ReStart();

		}for(var i = 0; i<PuntosFlexo.Length; i++){

			PuntosFlexo[i].generarDistanciaDelPunto();

		}for(var i = 0; i<ZonasActivas.Length; i++){

			ZonasActivas[i].SetActive(true);

		}

		for(var i = 0; i<4; i++){
			
			Opciones[i].transform.Find("icono_error").GetComponent<Image>().enabled = false;
			Opciones[i+4].transform.Find("icono_error").GetComponent<Image>().enabled = false;

		}
		for(var i = 0; i<Trafos.Length; i++){

			Trafos[i].SetActive(false);

		}
		//Habilitar Cajas
		for(var i = 0; i< ZonasActivasBotones.Length; i++){

			ZonasActivasBotones[i].SetActive(HabilitarEdicion[randomSettings][i]);


		}

		TrafosList [0] = Trafos1;
		TrafosList [1] = Trafos2;
		TrafosList [2] = Trafos3;
		TrafosList [3] = Trafos4;

		for(var i = 0; i<TrafosList.Length; i++){

			for(var l = 0; l<Trafos1.Length; l++){

				if (i == randomSettings) {
					TrafosList [randomSettings] [l].SetActive (true); 
				} else {

					TrafosList [i] [l].SetActive (false);

				}
			}
		}
	}
}
