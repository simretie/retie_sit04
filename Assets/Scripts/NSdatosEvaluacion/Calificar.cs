﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using NSBoxMessage;
using NSInterfaz;
using System;
using System.Globalization;
using NSScore;
using NSTraduccionIdiomas;

public class Calificar : MonoBehaviour
{
    [SerializeField] private InputField[] todoLosinputDelapractica;
    public MangerZonas mgZonas;

    public TMP_Dropdown[] RespuestasUsuario;

    #region members

    public ControladorDeDatos ConTDatos;
    public EvaluadorDeZona[] zonasEvaluadas;

    public PanelTransformado TransformadorZona5;

    public ManagerZona5 mgZona5;

    /// <summary>
    /// es la nota sobra al cual se va a dar la maxima calificacion 
    /// </summary>
    public float Notabase;

    public float NotaZonas;
    public float NotaZona5;
    public float NotaIntetos;
    public float NotaPregustasEvaluacion;

    public Text[] InputDelRegistroDeDaros;
    public GameObject[] XInputDelRegistro;
    public InputField[] InputDelRegistro;
    public bool HayPregutnasSinResponder = false;

    [SerializeField] private PanelRegistroDeDatos refPanelRegistroDeDatos;

    #endregion

    #region accesors

    #endregion

    #region monoBehaviour

    private void Start()
    {
        //ConTDatos.SetContarTiempo(true);
    }

    // Update is called once per frame
    void Update()
    {
    }

    #endregion

    #region private methods

    private bool ValidarEmptyInputs()
    {
        var tmpEmptyInputs = 0;

        for (int i = 0; i < todoLosinputDelapractica.Length; i++)
        {
            if (todoLosinputDelapractica[i].text.Equals(""))
                tmpEmptyInputs++;
        }

        return tmpEmptyInputs > 0;
    }

    #endregion

    #region public methods

    public void validar(bool pdf)
    {
        float aux = 0f;
        NotaZonas = 0;
        int tam = zonasEvaluadas.Length;
        for (int i = 0; i < tam; i++)
        {
            aux = zonasEvaluadas[i].Get_valorPorcentajeCorreto();
            Debug.Log(NotaZonas + "--> zona" + i + "->" + aux);
            NotaZonas = aux + NotaZonas;
        }

        if (ValidarEmptyInputs())
        {
            ConTDatos.AddIntentos();
            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("MensajeCamposNecesariosVerificar", "Todos los campos son necesarios, por favor verifique los datos y vuelva a intentarlo."), DiccionarioIdiomas._instance.Traducir("MensajeValorIncorrectoPinzaTelurometroAceptar", "ACEPTAR"));
            return;
        }

        Debug.Log(NotaZonas + " nota antes de evaluar si esta bien el registro");
        validarCamposInput();

        Debug.Log(NotaZonas + " nota antes de evaluar si esta bien el registro");
        if (NotaZonas <= 0.46)
        {
            if (pdf)
                MensajeValidarPdf(false);
            else
                MensajeCunadoSedaValidar(false);
        }
        else
        {
            if (pdf)
                MensajeValidarPdf(true);
            else
                MensajeCunadoSedaValidar(true);
        }

        validarZona5();
        validarNotaPorIntentos();
		// ConTDatos.AddIntentos();
		/* if (!HayPregutnasSinResponder)
		 {
				 BoxMessageManager._instance.MtdCreateBoxMessageInfo("Se han quedado campos sin llenar porfavor verifiue el registro de datos","ACEPTAR");
		 }*/
	}

    public void validarRetie2(bool state)
    {
        validarNotaPorIntentosRetie2();
        float nota = GetComponent<CalificarRetie2>().VerificarDatosUsuario() + (NotaPregustasEvaluacion * 0.1f) + NotaIntetos;

        GetComponent<CalificarRetie2>().NotaTotal = nota;

        if (GetComponent<CalificarRetie2>().EmptyCampos)
        {
            ConTDatos.AddIntentos();
            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("MensajeCamposNecesariosVerificar", "Todos los campos son necesarios, por favor verifique los datos y vuelva a intentarlo."), DiccionarioIdiomas._instance.Traducir("MensajeValorIncorrectoPinzaTelurometroAceptar", "ACEPTAR"));
            return;
        }

        if (!GetComponent<CalificarRetie2>().EmptyCampos && !GetComponent<CalificarRetie2>().bientodos && !state)
        {
            ConTDatos.AddIntentos();
            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("", "Los datos ingresados no son correctos, por favor revise el desarrollo de la actividad e intente de nuevo."), DiccionarioIdiomas._instance.Traducir("MensajeValorIncorrectoPinzaTelurometroAceptar", "ACEPTAR"));
            return;
        }
        else if (GetComponent<CalificarRetie2>().bientodos && !state)
        {
            BoxMessageManager._instance.MtdCreateBoxMessageInfo("Felicitaciones, ha registrado los valores de acuerdo al reglamento RETIE, a continuación puede generar el reporte de práctica.", DiccionarioIdiomas._instance.Traducir("MensajeValorIncorrectoPinzaTelurometroAceptar", "ACEPTAR"));
        }

        if (!GetComponent<CalificarRetie2>().bientodos && state)
        {
            ConTDatos.AddIntentos();
            MensajeValidarRetie2(false);
        }
        else if (state)
        {
            MensajeValidarRetie2(true);
        }

        //PanelPreguntas.Mostrar (true);
    }

    public void validarRetie3(bool state)
    {
        validarNotaPorIntentosRetie3();
        float nota = GetComponent<CalificarRetie3>().VerificarDatosUsuario() + (NotaPregustasEvaluacion * 0.1f) + NotaIntetos;

        GetComponent<CalificarRetie3>().NotaTotal = nota;

        if (GetComponent<CalificarRetie3>().EmptyCampos)
        {
            ConTDatos.AddIntentos();
            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("MensajeCamposNecesariosVerificar", "Todos los campos son necesarios, por favor verifique los datos y vuelva a intentarlo."), DiccionarioIdiomas._instance.Traducir("MensajeValorIncorrectoPinzaTelurometroAceptar", "ACEPTAR"));
            return;
        }

        if (!GetComponent<CalificarRetie3>().EmptyCampos && !GetComponent<CalificarRetie3>().camposCorrectos && !state)
        {
            ConTDatos.AddIntentos();
            BoxMessageManager._instance.MtdCreateBoxMessageInfo("Los datos ingresados no son correctos, por favor revise el desarrollo de la actividad e intente de nuevo.", DiccionarioIdiomas._instance.Traducir("MensajeValorIncorrectoPinzaTelurometroAceptar", "ACEPTAR"));
            return;
        }
        else if (GetComponent<CalificarRetie3>().camposCorrectos && !state)
        {
            BoxMessageManager._instance.MtdCreateBoxMessageInfo("Felicitaciones, ha registrado los valores de acuerdo al reglamento RETIE, a continuación puede generar el reporte de práctica.", DiccionarioIdiomas._instance.Traducir("MensajeValorIncorrectoPinzaTelurometroAceptar", "ACEPTAR"));
        }

        if (!GetComponent<CalificarRetie3>().camposCorrectos && state)
        {
            ConTDatos.AddIntentos();
            MensajeValidarRetie3(false);
        }
        else if (state)
        {
            ConTDatos.AddIntentos();
            MensajeValidarRetie3(true);
        }

        //PanelPreguntas.Mostrar (true);
    }

    public void validarRetie4(bool state)
	{

		EvaluarNota();

		CalificarRetie4 calificarRetie4 = GetComponent<CalificarRetie4>();
		if (calificarRetie4.EmptyCampos)
		{
			ConTDatos.AddIntentos();
			BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("MensajeCamposNecesariosVerificar", "Todos los campos son necesarios, por favor verifique los datos y vuelva a intentarlo."), DiccionarioIdiomas._instance.Traducir("MensajeValorIncorrectoPinzaTelurometroAceptar", "ACEPTAR"));
			return;
		}

		if (!calificarRetie4.EmptyCampos && !calificarRetie4.CamposCorrectos && !state)
		{
			ConTDatos.AddIntentos();
			BoxMessageManager._instance.MtdCreateBoxMessageInfo("Los datos ingresados no son correctos, por favor revise el desarrollo de la actividad e intente de nuevo.", DiccionarioIdiomas._instance.Traducir("MensajeValorIncorrectoPinzaTelurometroAceptar", "ACEPTAR"));
			return;
		}
		else if (calificarRetie4.CamposCorrectos && !state)
		{
			BoxMessageManager._instance.MtdCreateBoxMessageInfo("Felicitaciones, ha registrado los valores de acuerdo al reglamento RETIE, a continuación puede generar el reporte de práctica.", DiccionarioIdiomas._instance.Traducir("MensajeValorIncorrectoPinzaTelurometroAceptar", "ACEPTAR"));
		}

		if (!calificarRetie4.CamposCorrectos && state) // mínimo tiene los selectores bien y parte de los campos
		{
			ConTDatos.AddIntentos();
			MensajeValidarRetie4(false);
		}
		else if (state)
		{
			// ConTDatos.AddIntentos();
			MensajeValidarRetie4(true); // el mismo mensaje
		}

		//PanelPreguntas.Mostrar (true);
	}

	public void EvaluarNota()
	{
		CalificarRetie4 calificarRetie4 = GetComponent<CalificarRetie4>();
		validarNotaPorIntentosRetie4();
		float nota = calificarRetie4.VerificarDatosUsuario() + (NotaPregustasEvaluacion * 0.15f) + NotaIntetos;

		calificarRetie4.NotaTotal = nota;
	}

	public void MensajeCunadoSedaValidar(bool estaBien)
    {
        if (estaBien)
            BoxMessageManager._instance.MtdCreateBoxMessageInfo("Felicitaciones, ha registrado los valores de acuerdo al reglamento RETIE, a continuación puede generar el reporte de práctica.", DiccionarioIdiomas._instance.Traducir("MensajeValorIncorrectoPinzaTelurometroAceptar", DiccionarioIdiomas._instance.Traducir("MensajeValorIncorrectoPinzaTelurometroAceptar", "ACEPTAR")), GenerarReporte);
        else
            BoxMessageManager._instance.MtdCreateBoxMessageDecision("Los datos ingresados no son correctos, revise los datos y haga clic de nuevo en el botón verificar. ¿Desea generar el reporte con estos datos?", DiccionarioIdiomas._instance.Traducir("TextBotonRegistroDatosReporte", "REPORTE"), "CANCELAR", GenerarReporte);
    }

    private void GenerarReporte()
    {
        mgZonas.PasarAgenerarReporte();
        refPanelRegistroDeDatos.Mostrar(false);
    }

    public void RespuestaReporte()
    {
        // generar reporte

        GameObject.FindObjectOfType<ManagerRetieZonas2>().PasarAgenerarReporte();
    }

    public void RespuestaReporteR3()
    {
        // generar reporte

        GameObject.FindObjectOfType<ManagerRetieZonas2>().PasarAgenerarReporteRT3();
    }

    public void RespuestaReporteR4()
    {
        // generar reporte

        GameObject.FindObjectOfType<ManagerRetieZonas2>().PasarAgenerarReporteRT4();
    }

    public void MensajeValidarPdf(bool noMostrar)
    {
        if (noMostrar)
        {
            BoxMessageManager._instance.MtdCreateBoxMessageInfo("Felicitaciones, ha registrado los valores de acuerdo al reglamento RETIE, a continuación puede generar el reporte de práctica", DiccionarioIdiomas._instance.Traducir("MensajeValorIncorrectoPinzaTelurometroAceptar", DiccionarioIdiomas._instance.Traducir("MensajeValorIncorrectoPinzaTelurometroAceptar", "ACEPTAR")));
        }
        else
        {
            BoxMessageManager._instance.MtdCreateBoxMessageInfo("Los datos ingresados no son correctos, por favor revise el desarrollo de la actividad e intente de nuevo.", DiccionarioIdiomas._instance.Traducir("MensajeValorIncorrectoPinzaTelurometroAceptar", DiccionarioIdiomas._instance.Traducir("MensajeValorIncorrectoPinzaTelurometroAceptar", "ACEPTAR")));
        }
    }

    public void MensajeValidarRetie2(bool estaBien)
    {
        if (estaBien)
        {
            BoxMessageManager._instance.MtdCreateBoxMessageDecision("Felicitaciones, ha registrado los valores de acuerdo al reglamento RETIE, a continuación puede generar el reporte de práctica", DiccionarioIdiomas._instance.Traducir("TextBotonRegistroDatosReporte", "REPORTE"), DiccionarioIdiomas._instance.Traducir("TextBotonCancelar", "CANCELAR"), RespuestaReporte);
        }
        else
        {
            BoxMessageManager._instance.MtdCreateBoxMessageDecision("Los datos registrados no son correctos, revise los datos y haga clic de nuevo en el botón verificar. ¿Desea generar el reporte con estos datos?", DiccionarioIdiomas._instance.Traducir("TextBotonRegistroDatosReporte", "REPORTE"), DiccionarioIdiomas._instance.Traducir("TextBotonCancelar", "CANCELAR"), RespuestaReporte);
        }
    }

    public void MensajeValidarRetie3(bool estaBien)
    {
        if (estaBien)
        {
            BoxMessageManager._instance.MtdCreateBoxMessageDecision("Felicitaciones, ha registrado los valores de acuerdo al reglamento RETIE, a continuación puede generar el reporte de práctica", DiccionarioIdiomas._instance.Traducir("TextBotonRegistroDatosReporte", "REPORTE"), DiccionarioIdiomas._instance.Traducir("TextBotonCancelar", "CANCELAR"), RespuestaReporteR3);
        }
        else
        {
            BoxMessageManager._instance.MtdCreateBoxMessageDecision("Los datos registrados no son correctos, revise los datos y haga clic de nuevo en el botón verificar. ¿Desea generar el reporte con estos datos?", DiccionarioIdiomas._instance.Traducir("TextBotonRegistroDatosReporte", "REPORTE"), DiccionarioIdiomas._instance.Traducir("TextBotonCancelar", "CANCELAR"), RespuestaReporteR3);
        }
    }

    public void MensajeValidarRetie4(bool estaBien)
    {
        if (estaBien)
        {
            BoxMessageManager._instance.MtdCreateBoxMessageDecision("Felicitaciones, ha registrado los valores de acuerdo al reglamento RETIE, a continuación puede generar el reporte de práctica", DiccionarioIdiomas._instance.Traducir("TextBotonRegistroDatosReporte", "REPORTE"), DiccionarioIdiomas._instance.Traducir("TextBotonCancelar", "CANCELAR"), RespuestaReporteR4);
        }
        else
        {
            BoxMessageManager._instance.MtdCreateBoxMessageDecision("Los datos registrados no son correctos, revise los datos y haga clic de nuevo en el botón verificar. ¿Desea generar el reporte con estos datos?", DiccionarioIdiomas._instance.Traducir("TextBotonRegistroDatosReporte", "REPORTE"), DiccionarioIdiomas._instance.Traducir("TextBotonCancelar", "CANCELAR"), RespuestaReporteR4);
        }
    }

    public void reiniciarRegistroDeDatos()
    {
        var tam = RespuestasUsuario.Length;
        for (int i = 0; i < tam; i++)
        {
            RespuestasUsuario[i].value = 0;
            RespuestasUsuario[i].transform.GetChild(0).gameObject.SetActive(false); //GetComponent<Image>().color = new Color(1f, 1f, 1f);
        }

        InputDelRegistro[0].text = "";
        InputDelRegistro[1].text = "";
        XInputDelRegistro[0].SetActive(false);
        XInputDelRegistro[1].SetActive(false);
        NotaZonas = 0;
    }

    public void validarZona5()
    {
        float kva = float.Parse(TransformadorZona5.Kvea.text);
        int pesoTor = Convert.ToInt32(TransformadorZona5.MasaTotalTransformador.text);

        switch (mgZona5.ElPosteElegidoEsEnH())
        {
            case -1:
                NotaZona5 = 0;
                Debug.Log("notazona5=0");
                break;
            case 0:
                if (pesoTor < 750)
                {
                    NotaZona5 = 0.15f;
                    Debug.Log("notazona5=0.15");
                }
                else
                {
                    NotaZona5 = 0f;
                    Debug.Log("notazona5=0 ");
                }

                break;
            case 1:
                if (pesoTor >= 750)
                {
                    NotaZona5 = 0.15f;
                    Debug.Log("notazona5=0.15");
                }
                else
                {
                    NotaZona5 = 0f;
                    Debug.Log("notazona5=0 ");
                }

                break;
        }
    }

    public void validarNotaPorIntentos()
    {
        if (ConTDatos._cantidadIntentos - 1 >= 10)
        {
            NotaIntetos = 0.05f;
        }
        else
        {
            NotaIntetos = 0.2f - (0.015f * (ConTDatos._cantidadIntentos - 1f));
        }
    }

    public void validarNotaPorIntentosRetie2()
    {
        if (ConTDatos._cantidadIntentos - 1 >= 10)
        {
            NotaIntetos = 0.05f;
        }
        else
        {
            NotaIntetos = 0.1f - (0.01f * (ConTDatos._cantidadIntentos - 1f));
        }
    }

    public void validarNotaPorIntentosRetie3()
    {
        if (ConTDatos._cantidadIntentos - 1 >= 10)
        {
            NotaIntetos = 0.05f;
        }
        else
        {
            NotaIntetos = 0.15f - (0.01f * (ConTDatos._cantidadIntentos - 1f));
        }
    }

    public void validarNotaPorIntentosRetie4()
    {
		int cantidadIntentos = ConTDatos._cantidadIntentos + 1;
		if (cantidadIntentos - 1 >= 10)
        {
            NotaIntetos = 0.05f;
        }
        else
        {
			NotaIntetos = 0.15f - (0.01f * (cantidadIntentos - 1f));
        }
    }

    public void validarCamposInput()
    {
        if (InputDelRegistroDeDaros[0].text != null && InputDelRegistroDeDaros[1].text != null)
        {
            if (InputDelRegistroDeDaros[1].text.Equals(TransformadorZona5.Kvea.text))
            {
                NotaZonas = NotaZonas + 0.015625f;
                XInputDelRegistro[0].SetActive(false);
                Debug.Log("entre a Kva");
            }
            else
            {
                XInputDelRegistro[0].SetActive(true);
            }

            if (InputDelRegistroDeDaros[0].text.Equals(TransformadorZona5.MasaTotalTransformador.text))
            {
                NotaZonas = NotaZonas + 0.015625f;
                Debug.Log("peso del transformador");
                XInputDelRegistro[1].SetActive(false);
            }
            else
            {
                XInputDelRegistro[1].SetActive(true);
            }
        }
        else
        {
            HayPregutnasSinResponder = true;
        }
    }

    public void AsignarPreguntasEvaluacionCorrectas(float notaEvaluacion)
    {
        NotaPregustasEvaluacion = notaEvaluacion;
    }

    public string GetCalificacionTotalRango()
    {
        var tmpCalificacion = "";
        if (GetComponent<CalificarRetie2>() != null)
        {
            tmpCalificacion = FindObjectOfType<ScoreController>().GetScoreFromFactor01(GetComponent<CalificarRetie2>().NotaTotal);
            Debug.Log("Calificación zona 2: " + tmpCalificacion);
            return tmpCalificacion;
        }

        if (GetComponent<CalificarRetie3>() != null)
        {
            tmpCalificacion = FindObjectOfType<ScoreController>().GetScoreFromFactor01(GetComponent<CalificarRetie3>().NotaTotal);
            Debug.Log("Calificación zona 3: " + tmpCalificacion);
            return tmpCalificacion;
        }

        if (GetComponent<CalificarRetie4>() != null)
        {
			float notaTotal = GetComponent<CalificarRetie4>().NotaTotal;
			tmpCalificacion = FindObjectOfType<ScoreController>().GetScoreFromFactor01(Mathf.Clamp01(notaTotal));
            Debug.Log("Calificación zona 4: " + tmpCalificacion);
            return tmpCalificacion;
        }

        tmpCalificacion = FindObjectOfType<ScoreController>().GetScoreFromFactor01(NotaZonas + NotaZona5 + (NotaIntetos + (0.15f * NotaPregustasEvaluacion)));
        return tmpCalificacion;
    }

    public float GetCalificacionTotal()
    {
        if (GetComponent<CalificarRetie2>() != null)
        {
            return 2 * GetComponent<CalificarRetie2>().NotaTotal; // 2 porque mi base no es 1 si no 0.5
        }
        else if (GetComponent<CalificarRetie3>() != null)
        {
            return 2 * GetComponent<CalificarRetie3>().NotaTotal;
        }
        else if (GetComponent<CalificarRetie4>() != null)
        {
            return GetComponent<CalificarRetie4>().NotaTotal;
        }
        else
        {
            return NotaZonas + NotaZona5 + NotaIntetos + (0.15f * NotaPregustasEvaluacion);
        }
    }

    #endregion
}