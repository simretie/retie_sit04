﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Graficadora : MonoBehaviour {

	// Use this for initialization
	public Transform[] PuntosGrafica;
	public LineRenderer LineaPuntos;
	public TMPro.TextMeshPro[] TextosResitencias;
	public TMPro.TextMeshPro[] TextosDistancias;

	public TMPro.TextMeshPro[] resistenciasText;
	public Transform RootDatos;
	public Transform UltimoPunto;
 
	private bool MostrandoGrafica = false;
	public InputField inputHeight;
	private int RandomHeight;

	public TMPro.TextMeshPro Resistencia61;

	[Header("Datos verticales grafica")]
	public TMPro.TextMeshPro[] datosVerticales;
	private float[] datosTemp = new float[20];
	private float[] distanciasDV;
	public int datosIntermedios = 0;
	private bool calculandoDatos = false;

	void Start () {

		RandonHeightZona1 ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		UpdateLineRenderer ();

	}public void SetDatosGraficar(float[] datos,float[] distancias){

		for(var i = 0; i<datosTemp.Length; i++){

			if(i < datos.Length){
				datosTemp[i] = datos[i];
			}else{

				datosTemp[i] = datos[0]+ ((datos[1]-datos[0]) * i);

			}
		}



		datosTemp = datos;
		float DistanciaFix = datos[0];  // elijo el primer dato para correr la grafíca con base en esa posición y que muevan correctamente

		for(var i = 0; i<PuntosGrafica.Length; i++){

			PuntosGrafica[i].transform.localPosition = new Vector3(i+1,datos[i]-DistanciaFix,0);
			TextosResitencias[i].text = datos[i].ToString("F2");
			TextosDistancias[i].text = distancias[i].ToString("F2");
			Resistencia61.text = datos[5].ToString("F2");

		}

		resistenciasText[0].text = datos[0].ToString("F2");
		resistenciasText[1].text = datos[datos.Length-1].ToString("F2");


	}public void UpdateLineRenderer(){

		for(var i= 0; i<LineaPuntos.positionCount; i++){

			LineaPuntos.SetPosition(i,PuntosGrafica[i].transform.position);

		}

		resistenciasText[1].gameObject.transform.localPosition = new Vector3(resistenciasText[0].gameObject.transform.localPosition.x,UltimoPunto.transform.localPosition.y*RootDatos.transform.localScale.y,resistenciasText[0].gameObject.transform.localPosition.z);

		if(MostrandoGrafica)
			AjustarDatosVerticales();

	}public void Mostrar(){

		MostrandoGrafica = !MostrandoGrafica;
		GetComponent<Animator>().SetBool("open",MostrandoGrafica);

	}public void RandonHeightZona1(){

		RandomHeight = Random.Range(5,10);
		inputHeight.text = RandomHeight.ToString();

	}public int GetHeightValue(){

		return RandomHeight;

	}public void AjustarDatosVerticales(){

		if(!calculandoDatos){

			calculandoDatos = true;

			distanciasDV = new float[datosVerticales.Length];
			datosVerticales[1].text = datosTemp[0].ToString("F2");

			float DistanciaInterna = 0f;
			int correcto = 0;
			float lastNum = 0f;
			int tempCor = 0;
				  
			for(var i = 0; i<datosVerticales.Length;i++){

				distanciasDV[i] = Vector3.Distance(
					new Vector3(0, datosVerticales[i].transform.localPosition.y,0),
					new Vector3(0, resistenciasText[1].transform.localPosition.y,0));

			}

			lastNum = distanciasDV[0];
			for(var i = 0; i<datosVerticales.Length-1;i++){

				if(lastNum >= distanciasDV[i]){

					datosIntermedios +=1;
					lastNum = distanciasDV[i];
					tempCor = i;

				}else{

					// este es el menor, los demás empiezan nuevamente a incrementarse


				}
	 
			}

			float TmpDistanciaInterna =datosTemp[tempCor]-datosTemp[0];
			DistanciaInterna =  TmpDistanciaInterna/tempCor;

			datosVerticales[0].text = (datosTemp[0] - DistanciaInterna).ToString("F2");

			for(var i = 2; i<datosVerticales.Length; i++){

				if(i!=tempCor)
					datosVerticales[i].text = (datosTemp[0] + (DistanciaInterna*i)).ToString("F2");

			}

			datosVerticales[tempCor].text = datosTemp[datosTemp.Length-1].ToString("F2");
			datosIntermedios = 0;

			calculandoDatos = false;

		}
	}
}
