﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSInterfaz;

public class ManagerZona7 : MonoBehaviour {

    public panelPlacaPoste placaposte;
    public PanelInterfazMensaje panelEmporamiento;

    public PanelInterfazBienvenida explicaiconzona7;
    public TipodeConeccion ObCable;
    public GameObject placa;
    public GameObject interfazDeH;
    public GameObject poste;
    public GameObject flexomatro;
    public GameObject hueco;
    public bool zona7activa;
    private bool activarMensaje=true;

    // Use this for initialization
    void Start () {
        zona7activa = false;
	}
    private void OnEnable()
    {
        placaposte.gameObject.SetActive( false);
        panelEmporamiento.gameObject.SetActive( false);
        explicaiconzona7.Mostrar(activarMensaje);
    }
    // Update is called once per frame
    void Update () {

	}

    /// <summary>
    /// /inicia la zona 7 despues de definir el poste o la reinicia
    /// </summary>
    /// <param name="activar">true para iniciar, flase para reiniciar</param>
    public void SecuenciaZona7(bool activar)
    {
        zona7activa = activar;
        hueco.SetActive(!activar);
        placa.GetComponent<panelPlacaPoste>().Mostrar(false);
        interfazDeH.GetComponent<PanelInterfazMensaje>().Mostrar(false);
        activarMensaje = !activar;
        flexomatro.SetActive(activar);
        poste.SetActive(activar);
        ObCable.activarLinea();
    }


    public void reiniciar()
    {
        ObCable.reiniciar();
        reiniciarLetrero();
        SecuenciaZona7(false);
    
    }



    public void reiniciarLetrero()
    {
        activarMensaje = true;
    }
}
