﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSAvancedUI;
using UnityEngine.UI;
using NSInterfaz;

public class panelZonas : AbstractPanelUIAnimation
{


    #region members


    [SerializeField]
    private Camera camara;
    private RawImage zona;


    [SerializeField]
    private bool tieneTempleteVariable;

    [SerializeField]
    private GameObject templeteGuitarra;

    [SerializeField]
    private puntosEvaluacion EvGuitarra;

    //--------------------

    [SerializeField]
    private GameObject templeteDirecto;
    [SerializeField]
    private puntosEvaluacion EvDirecto;


    public bool ElTempleteEsguitarra;

    #endregion

    #region accesors

    #endregion

    #region events

    #endregion

    #region monoBehaviour

    private void Start()
    {
       //if(tieneTempleteVariable)
         //   decidirtemplete();
    }

    private void OnDisable()
    {
        
    }
    #endregion

    #region private methods

    public void decidirtemplete()
    {
        if (tieneTempleteVariable)
        {
            int var = Random.Range(0, 50);
            if (var <= 25)
            {
                templeteGuitarra.SetActive(true);
                templeteDirecto.SetActive(false);
                EvDirecto.activo = false;
                EvGuitarra.activo = true;
            }
            else
            {
                templeteGuitarra.SetActive(false);
                templeteDirecto.SetActive(true);
                EvDirecto.activo = true;
                EvGuitarra.activo = false;
            }
        }
    }
    
    #endregion

    #region public methods


    #endregion

    #region courutines

    #endregion
}
