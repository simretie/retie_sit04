﻿using NSSingleton;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSBoxMessage
{
    public class BoxMessageManager : AbstractSingleton<BoxMessageManager>
    {
        #region members
        /// <summary>
        /// Prefabricado del la caja de mensaje que muestra una informacion
        /// </summary>
        [SerializeField, Tooltip("Prefabricado del la caja de mensaje que muestra una informacion")]
        private GameObject prefabMessageInfo;
        /// <summary>
        /// Prefabricado de la caja de mensaje que muestra un mensaje y botones para tomar una decision
        /// </summary>
        [SerializeField, Tooltip("Prefabricado de la caja de mensaje que muestra un mensaje y botones para tomar una desicion")]
        private GameObject prefabMessageDecision;
        /// <summary>
        /// Prefabricado de la caja de mensaje que muestra una pequeña informacion y desaparece
        /// </summary>
        [SerializeField, Tooltip("Prefabricado de la caja de mensaje que muestra una pequeña informacion y desaparece ")]
        private GameObject prefabMessageMini;
        /// <summary>
        /// Canvas en donde se mostraran las cajas de mensaje
        /// </summary>
        [SerializeField, Tooltip("Canvas en donde se mostraran las cajas de mensaje")]
        private Transform canvas;

        private GameObject boxMessageDesicionActual;

        private GameObject boxMessageInfoActual;

        private GameObject boxMessageMiniActual;
        #endregion

        #region methods

        /// <summary>
        /// Crea una nueva caja de mensaje para tomar una decision, con texto en sus botones
        /// </summary>
        /// <param name="argTextMessage">Mensaje que se mostrara</param>
        /// <param name="argTextButtonAccept">Texto que se colocara en el boton aceptar</param>
        /// <param name="argTextButtonCancel">Texto que se colocara en el boton cancelar</param>
        /// <param name="argFunctionExeAccept">Funcion que se ejecutara si el usuario acepta</param>
        /// <param name="argFunctionExeCancel">Funcion que se ejecutara si el usuario cancela</param>
        public void MtdCreateBoxMessageDecision(string argTextMessage, string argTextButtonAccept, string argTextButtonCancel, delegateOnBoxMessageButtonAccept argFunctionExeAccept = null, delegateOnBoxMessageButtonCancel argFunctionExeCancel = null)
        {
            boxMessageDesicionActual = Instantiate(prefabMessageDecision, canvas);

            var tmpBoxMessage = boxMessageDesicionActual.GetComponent<BoxMessageDecision>();
            tmpBoxMessage._textMessage = argTextMessage;
            tmpBoxMessage._textButtonAccept = argTextButtonAccept;
            tmpBoxMessage._textButtonCancel = argTextButtonCancel;

            if (argFunctionExeAccept == null)   
                tmpBoxMessage.mtdAddDelegateButtonAccept(delegate(){ });
            else
                tmpBoxMessage.mtdAddDelegateButtonAccept(argFunctionExeAccept);

            if (argFunctionExeCancel == null)
                tmpBoxMessage.mtdAddDelegateButtonAccept(delegate () { });
            else
                tmpBoxMessage.mtdAddDelegateButtonCancel(argFunctionExeCancel);
        }

        /// <summary>
        /// Crea una nueva caja de mensaje informativo que el usuario debe de cerrar precionando un boton de aceptar
        /// </summary>
        /// <param name="argTextMessage">Mensaje que se mostrara</param>
        /// <param name="argTextButtonAccept">Texto del mensaje del boton aceptar, o ok</param>
        /// <param name="argFunctionExeAccept">Funcion que se ejecutara si el usuario acepta</param>
        public GameObject MtdCreateBoxMessageInfo(string argTextMessage, string argTextButtonAccept, delegateOnBoxMessageButtonAccept argFunctionExeAccept = null, bool argActivarBotonCerrar = false)
        {
            if (boxMessageInfoActual)
                Destroy(boxMessageInfoActual);

            boxMessageInfoActual = Instantiate(prefabMessageInfo, canvas);

            var tmpBoxMessage = boxMessageInfoActual.GetComponent<BoxMessageInfo>();
            tmpBoxMessage._textMessage = argTextMessage;
            tmpBoxMessage._textButtonAccept = argTextButtonAccept;
            tmpBoxMessage.ActivarButtonCerrar(argActivarBotonCerrar);

            if (argFunctionExeAccept == null)
                tmpBoxMessage.mtdAddDelegateButtonAccept(delegate(){ });
            else
                tmpBoxMessage.mtdAddDelegateButtonAccept(argFunctionExeAccept);

            return boxMessageInfoActual;
        }

        /// <summary>
        /// Crea un pequeño mensaje que se destruye en un tiempo determinado
        /// </summary>
        /// <param name="argTextMessage">Mensaje que se mostrara</param>
        /// <param name="argTimeDestroy">Tiempo en el que se destruira el mensaje</param>
        public void MtdCreateBoxMessageMini(string argTextMessage, float argTimeDestroy = -1)
        {
            if (boxMessageMiniActual != null)            
                Destroy(boxMessageMiniActual);            

            boxMessageMiniActual = Instantiate(prefabMessageMini, canvas);

            var tempBoxMessage = boxMessageMiniActual.GetComponent<BoxMessageMini>();
            tempBoxMessage._textMessage = argTextMessage;
            tempBoxMessage._timeForDestroy = argTimeDestroy;
        }
        #endregion
    }
}