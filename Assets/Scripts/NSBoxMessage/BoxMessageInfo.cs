﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSBoxMessage
{
    public class BoxMessageInfo : AbstractBoxMessage
    {
        [SerializeField]
        private GameObject buttonCerrar;

        public void ActivarButtonCerrar(bool argActivar)
        {
            buttonCerrar.SetActive(argActivar);
        }
    }
}