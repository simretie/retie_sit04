﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;
using System.Diagnostics;
using System.Timers;
using System.Threading;
using System;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;

public class Utilidades
{
    //VARAIBLES

    //----------------------
    //---APP NAME
    const string COMPANY = "CloudLabs";

    //SPA
    static string PRODUCT_SPA = "CloudLabs - Simulador de inspección de requerimientos RETIE y distancias de seguridad en una sub-estación eléctrica interior";
    static string PRODUCT_SPA_OSX = "CloudLabs - Aplicabilidad RETIE";
    static string PRODUCT_ENG = "CloudLabs";
    static string PRODUCT_POR = "CloudLabs";

    

    //----------------------
    //---identifier
    static string SPA_ID = "com.cloudlabs.retie.requerimientos.v4";
    static string ENG_ID = "com.cloudlabs.eng.perdidas.retie";
    static string POR_ID = "com.cloudlabs.integrales.por";


    //----------------------
    //---LANG
    /*static menu.LanguageList Lang_SPA = menu.LanguageList.Spanish;
    static menu.LanguageList Lang_ENG = menu.LanguageList.English;
    static menu.LanguageList Lang_POR = menu.LanguageList.Portuguese;*/
    static NSTraduccionIdiomas.ClsControladorIdiomas.idioma Lang_SPA = NSTraduccionIdiomas.ClsControladorIdiomas.idioma.espaniol;
    static NSTraduccionIdiomas.ClsControladorIdiomas.idioma Lang_ENG = NSTraduccionIdiomas.ClsControladorIdiomas.idioma.ingles;
    static NSTraduccionIdiomas.ClsControladorIdiomas.idioma Lang_POR = NSTraduccionIdiomas.ClsControladorIdiomas.idioma.portugues;

    //---BIL LANG
    /*static menu.ConfigureLanguageList SPA_ENG = menu.ConfigureLanguageList.Spanish_English;
    static menu.ConfigureLanguageList POR_ENG = menu.ConfigureLanguageList.English_Portuguese;*/

    //----
    static bool SECURITY;

    //keyword
    static string simKeyword = "perd_retie04";
    static string simKeyword_U = "perd_retie04_u";
    static string simKeyword_K12 = "perd_retie04_k";
    static string std_folder = @"DEPLOY\Release\STD\";
    static string mono_folder = @"DEPLOY\Release\MONO\";

    [MenuItem("IE/Seguridad")]
    public static void Seguridad()
    {

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = !GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch;
        SECURITY = GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch;
        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        if (GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch)
        {
            EditorUtility.DisplayDialog("SEGURIDAD", "SEGURIDAD ACTIVADA", "ok");
        }
        else
        {
            EditorUtility.DisplayDialog("SEGURIDAD", "NOT", "ok");
        }

    }


    [MenuItem("IE/Build")]
    static void Build()
    {
        PlayerPrefs.DeleteAll();

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = true;
        SECURITY = GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch;


        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().ModoAula = false;

        //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.U;

        //sim_SPA_webGL_free();

        sim_SPA_webGL();
        /*sim_ENG_webGL();*/

       // sim_SPA_webGL_aula();
        /*sim_ENG_webGL();*/

        sim_SPA_webGL_aula_nuevaurl();

        //PC -->----------------
        sim_SPA_PC("S");
        /*sim_SPA_PC("Mono");
        sim_ENG_PC("S");
        sim_ENG_PC("Mono");
        sim_BLG_PC("S");
        sim_BLG_PC("Mono");*/
        //----------------------

        //OSX <--
        sim_SPA_OSX("S");
        /*sim_SPA_OSX("Mono");
        sim_ENG_OSX("U");
        sim_ENG_OSX("Mono");*/

        //sim_SPA_APK("S");
        /*sim_SPA_APK("Mono");
        sim_ENG_APK("U");
        sim_ENG_APK_mono("U");*/

    }


    /*[MenuItem("IE/test")]
    static void test()
    {
        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_OSX_std.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);
    }*/


    static BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
    static void groupScenes()
    {


        buildPlayerOptionsScene.scenes = new[] { "Assets/Scenes/Login.unity",
            "Assets/Scenes/practica4.unity"};
    }

    //Exportaciones
    //PC - SPA
    static void sim_SPA_PC(string complexType)
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_SPA.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_SPA;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = true;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().LtiActivo = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());


        groupScenes();


        if (complexType == "S"){
            GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_spa_std/" + simKeyword + "_spa_std.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);

            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_std.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);

        }
        else
        {
            GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = true;    
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword + "_spa_mono/" + simKeyword + "_spa_mono.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);

            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_mono.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);
        }

        //----------------------------------------------------------------------------------------
        
    }
    static void sim_ENG_PC(string complexType)
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_ENG;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = true;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().LtiActivo = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());


        groupScenes();


        if (complexType == "S")
        {
            GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_eng_std/" + simKeyword + "_eng_std.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);

            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_ENG_std.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);

        }
        else
        {
            GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = true;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword + "_eng_mono/" + simKeyword + "_eng_mono.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);

            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_ENG_mono.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);
        }

        //----------------------------------------------------------------------------------------

    }
    static void sim_BLG_PC(string complexType)
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_ENG;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = true;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = true;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().LtiActivo = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());


        groupScenes();


        if (complexType == "S")
        {
            GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_blg_std/" + simKeyword + "_blg_std.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);

            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_BLG_std.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);

        }
        else
        {
            GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = true;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword + "_blg_mono/" + simKeyword + "_blg_mono.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);

            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_BLG_mono.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);
        }

        //----------------------------------------------------------------------------------------

    }


    //OSX - SPA
    static void sim_SPA_OSX(string complexType)
    {
        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_SPA.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_SPA;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = true;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().LtiActivo = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();




        //----------------------------------------------------------------------------------------

        if (complexType == "S")
        {
            GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_spa_s_x/" + PRODUCT_SPA_OSX + ".app", BuildTarget.StandaloneOSX, BuildOptions.CompressWithLz4HC);



            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_OSX_std.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);

        }
        else
        {
            GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = true;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword + "_spa_m_x/" + PRODUCT_SPA_OSX + ".app", BuildTarget.StandaloneOSX, BuildOptions.CompressWithLz4HC);



            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_OSX_mono.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);
        }
    }


    //APK - SPA
    static void sim_SPA_APK(string complexType)
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_SPA.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, SPA_ID);
        EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Internal;

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_SPA;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = true;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().LtiActivo = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();



        if (complexType == "S")
        {
            GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_spa_std.apk", BuildTarget.Android, BuildOptions.CompressWithLz4HC);


        }
        else
        {
            GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = true;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword + "_spa_mono.apk", BuildTarget.Android, BuildOptions.CompressWithLz4HC);


        }
        //----------------------------------------------------------------------------------------
    }


    //[MenuItem("IE/sim_SPA_webGL")]
    static void sim_SPA_webGL()
    {
        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;
        /**/
        //PlayerSettings.WebGL.linkerTarget = WebGLLinkerTarget.Wasm;
        //PlayerSettings.WebGL.exceptionSupport = WebGLExceptionSupport.ExplicitlyThrownExceptionsOnly;
        //PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Gzip;

        //PlayerSettings.WebGL.memorySize = 512;
        //PlayerSettings.WebGL.template = "Simuladores";

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_SPA;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().LtiActivo = true;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().ModoAula = false;


        //string path = std_folder + simKeyword + "_spa_lti/" + simKeyword + "_spa_lti";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_spa_lti/" + simKeyword + "_spa_lti/"+"Html", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_std_weblti.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/

    }

    //[MenuItem("IE/sim_SPA_webGL")]
    static void sim_SPA_webGL_free()
    {
        /*PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;
        /**/
        //PlayerSettings.WebGL.linkerTarget = WebGLLinkerTarget.Wasm;
        //PlayerSettings.WebGL.exceptionSupport = WebGLExceptionSupport.ExplicitlyThrownExceptionsOnly;
        //PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Gzip;

        //PlayerSettings.WebGL.memorySize = 512;
        //PlayerSettings.WebGL.template = "Simuladores";

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_SPA;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().LtiActivo = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().ModoAula = false;


        //string path = std_folder + simKeyword + "_spa_lti/" + simKeyword + "_spa_lti";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_spa_lti_free/" + simKeyword + "_spa_lti_free/"+"Html", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_std_weblti_free.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/

    }


    //[MenuItem("IE/sim_SPA_webGL")]
    static void sim_SPA_webGL_aula()
    {
        /*PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;
        /**/
        //PlayerSettings.WebGL.linkerTarget = WebGLLinkerTarget.Wasm;
        //PlayerSettings.WebGL.exceptionSupport = WebGLExceptionSupport.ExplicitlyThrownExceptionsOnly;
        //PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Gzip;

        //PlayerSettings.WebGL.memorySize = 512;
        //PlayerSettings.WebGL.template = "Simuladores";

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_SPA;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().LtiActivo = false;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().WebAulaUrl = "http://servercloudlabs.com/validarSimulador/informacionUsuario";//esta variable es la url entonces lo que uno hace es asignarla aqui directamnte
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().ModoAula = true;


        //string path = std_folder + simKeyword + "_spa_lti/" + simKeyword + "_spa_lti";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_spa_lti_webAula/" + simKeyword + "_spa_lti_webAula/" + "Html", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_std_webAula.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().ModoAula = false;
        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());


    }

    //[MenuItem("IE/sim_SPA_webGL")]
    static void sim_SPA_webGL_aula_nuevaurl()
    {
        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;
        /**/
        //PlayerSettings.WebGL.linkerTarget = WebGLLinkerTarget.Wasm;
        //PlayerSettings.WebGL.exceptionSupport = WebGLExceptionSupport.ExplicitlyThrownExceptionsOnly;
        //PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Gzip;

        //PlayerSettings.WebGL.memorySize = 512;
        //PlayerSettings.WebGL.template = "Simuladores";

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_SPA;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().LtiActivo = false;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().WebAulaUrl = "https://menu.cloudlabs.us/validarSimulador/informacionUsuario";//esta variable es la url entonces lo que uno hace es asignarla aqui directamnte
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().ModoAula = true;


        //string path = std_folder + simKeyword + "_spa_lti/" + simKeyword + "_spa_lti";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();
        //cambias el nombre de las carpetas que se generan y del .bat
        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_spa_lti_webAula_nuevaurl/" + simKeyword + "_spa_lti_webAula_nuevaurl/" + "Html", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_std_webAula_nuevaurl.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/
 
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().ModoAula = false;
        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());
    }





    /*[MenuItem("IE/TEST")]
    static void sim_TEST()
    {
        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_std_weblti.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

    }*/
}